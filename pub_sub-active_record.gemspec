# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pub_sub/active_record/version'

Gem::Specification.new do |spec|
  spec.name          = "pub_sub-active_record"
  spec.version       = PubSub::ActiveRecord::VERSION
  spec.authors       = ["Dmitry Boltrushko"]
  spec.email         = ["dboltrushko@cloudnexa.com"]
  spec.summary       = %q{Pub/Sub ActiveRecord extension.}
  spec.description   = %q{Pub/Sub ActiveRecord extension.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "pub_sub"
  spec.add_dependency 'activerecord'
  spec.add_dependency 'activesupport'

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rspec_junit_formatter"
  spec.add_development_dependency "sqlite3"

end
