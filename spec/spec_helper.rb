require "rubygems"
require "bundler"
Bundler.setup(:default, :test, :development)

require 'active_record'
require 'active_record/connection_adapters/sqlite3_adapter'
require 'stringio'        # silence the output

#$stdout = StringIO.new    # from migrator
ActiveRecord::Base.establish_connection(
    :adapter => 'sqlite3',
    #:database => File.dirname(__FILE__) + "/db.sqlite3"
    :database => ':memory:'
)
ActiveRecord::Migrator.migrate(File.expand_path('../migrations', __FILE__))
#$stdout = STDOUT

require 'pub_sub/active_record'

Dir[File.join(File.dirname(__FILE__),"support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.run_all_when_everything_filtered = true
  config.filter_run :focus
  config.order = 'random'
end
