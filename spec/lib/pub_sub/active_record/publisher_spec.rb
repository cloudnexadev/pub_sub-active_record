require 'spec_helper'

describe PubSub::ActiveRecord::Publisher do

  describe '#pub_sub_publish' do
    let(:options) { {} }
    subject { article_class(options).new }

    it 'allows skipping CRUD events' do
      art = article_class(:skip_defaults => true)
      expect(art).to_not include(PubSub::ActiveRecord::Creation)
      expect(art).to_not include(PubSub::ActiveRecord::Update)
      expect(art).to_not include(PubSub::ActiveRecord::Destruction)
    end

    describe 'default options' do
      subject { article_class }

      specify { expect(subject).to include(PubSub::ActiveRecord::Creation) }
      specify { expect(subject).to include(PubSub::ActiveRecord::Destruction) }
      specify { expect(subject).to include(PubSub::ActiveRecord::Update) }

      # specify do
      #   expect(subject._commit_callbacks.select { |c| c.kind.eql?(:after) && c.name.eql?(:commit) }).to_not be_empty
      # end
      # specify { expect(subject._update_callbacks.select { |c| c.kind.eql?(:after) }).to_not be_empty }
      # specify { expect(subject._destroy_callbacks.select { |c| c.kind.eql?(:before) }).to_not be_empty }
    end

  end

end