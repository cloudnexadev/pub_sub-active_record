class User < ActiveRecord::Base; end

def article_class(options = {})
  Class.new(ActiveRecord::Base) do
    include PubSub::ActiveRecord::Publisher
    self.table_name = 'articles'

    pub_sub_publish options

    belongs_to :user
  end
end
