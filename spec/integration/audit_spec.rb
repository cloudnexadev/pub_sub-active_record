require 'spec_helper'

describe 'Audit' do

  subject(:article) { article_class.new }
  let(:spy) {SpyListener.new}

  before do
    PubSub.add_subscription(spy, with: :on_event_published)
  end

  after do
    PubSub.reset
  end

  it 'captures all CRUD events' do
    article.name = 'first'
    article.save!

    article.name = 'second'
    article.save!

    article.destroy!

    expect(spy.logs.size).to eq(3)
    #puts spy.logs.inspect
  end

end
