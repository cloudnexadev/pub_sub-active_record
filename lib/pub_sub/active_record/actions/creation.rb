module PubSub
  module ActiveRecord

    # Handles creation of Activities upon destruction and update of tracked model.
    module Creation
      extend ActiveSupport::Concern

      included do
        after_commit :broadcast_created, :on => :create

        private

        def broadcast_created
          publish(:created)
        end
      end
    end

  end
end