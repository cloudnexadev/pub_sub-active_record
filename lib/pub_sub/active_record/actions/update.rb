module PubSub
  module ActiveRecord

    # Handles creation of Activities upon destruction and update of tracked model.
    module Update
      extend ActiveSupport::Concern

      included do
        after_commit :broadcast_updated, :on => :update

        private

        def broadcast_updated
          publish(:updated)
        end
      end
    end

  end
end
