module PubSub
  module ActiveRecord

    # Handles publishing of event upon destruction of tracked model
    module Destruction
      extend ActiveSupport::Concern

      included do
        #before_destroy :broadcast_event
        after_commit :broadcast_destroyed, :on => :destroy

        private

        def broadcast_destroyed
          publish(:destroyed)
        end
      end
    end

  end
end