require 'active_support/concern'

module PubSub
  module ActiveRecord
    module Publisher
      extend ActiveSupport::Concern
      include ::PubSub::Publisher

      module ClassMethods

        def pub_sub_publish(opts = {})
          options = opts.clone

          include_default_events(options)

          # assign_globals       options
          # assign_hooks         options
          # assign_custom_fields options

          nil
        end

        def include_default_events(options)
          defaults = {
              create:  Creation,
              destroy: Destruction,
              update:  Update
          }

          if options[:skip_defaults] == true
            return
          end

          modules = if options[:except]
                      defaults.except(*options[:except])
                    elsif options[:only]
                      defaults.slice(*options[:only])
                    else
                      defaults
                    end

          modules.each do |key, value|
            include value
          end
        end

      end

    end
  end
end