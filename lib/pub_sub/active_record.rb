require "pub_sub/active_record/version"

require 'pub_sub'

require "pub_sub/active_record/actions/creation"
require "pub_sub/active_record/actions/update"
require "pub_sub/active_record/actions/destruction"

require "pub_sub/active_record/publisher"

# module PubSub
#   module ActiveRecord
#
#   end
# end
